import React from "react";
import Sidebar from "../Sidernavbar/Sidebar";
import Leftview from "./Leftview";
import Row from "../../../Component/Row";
import Col from "../../../Component/Col";
import Container from "../../../Component/Container";
import HeaderExample from "../Topheader/Header";

function Dashboard() {
  return (
    <Container fluid>
      <Row>
        <Col
          xs="1"
          md="1"
          className="left_pane padding-parentContainer backgroundV"
        >
          <Sidebar />
        </Col>
        <Col xs="11" md="11">
          <div style={{ borderBottom: "1px solid black" }}>
            {" "}
            <HeaderExample />
          </div>
          <Leftview />
        </Col>
      </Row>
    </Container>
  );
}

export default Dashboard;
