import React from "react";
import Text from "../../../../Component/Text";
import classnames from "classnames";
import styles from "./styles.module.css";
const spansizStyles = classnames(styles["spansiz"]);
const backgroudStyles = classnames(styles["background"]);
function Rightview(props) {
  return (
    <div>
      {props.views.map((itm, index) => {
        return (
          <div className={backgroudStyles} key={index}>
            <div className="text-center">
              <span className={spansizStyles}>
                {" "}
                {itm.firstname
                  .split(" ")
                  .map((name) => name.charAt(0))
                  .join("")}
              </span>
              <Text as="p" className="mt-3">
                {itm.firstname}
              </Text>
              <Text as="p">{itm.designation}</Text>
            </div>
            <div className="d-flex">
              <Text as="p">Fullname :</Text>
              <Text as="p">{itm.firstname}</Text>
            </div>

            <div className="d-flex">
              <Text as="p">Email :</Text>
              <Text as="p">{itm.email}</Text>
            </div>
            <div className="d-flex">
              <Text as="p">Phone :</Text>
              <Text as="p">{itm.phonenumber}</Text>
            </div>
            <div className="d-flex">
              <Text as="p">Company :</Text>
              <Text as="p">{itm.companyname}</Text>
            </div>
            <div className="d-flex">
              <Text as="p">Address :</Text>
              <Text as="p">{itm.Companyaddress}</Text>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default Rightview;
