import React, { useState } from "react";
import Row from "../../../../Component/Row";
import Col from "../../../../Component/Col";
import Container from "../../../../Component/Container";
import Form from "../../../../Component/Form";
import Button from "../../../../Component/Button";
import Modal from "../../../../Component/Modal";
import Text from "../../../../Component/Text";
import Checkbox from "../../../../Component/Checkbox/Checkbox";
import { ReactComponent as Notebook } from "../../../../Component/Icons/notebook.svg";
import Edit from "../../../../Component/Edit/Edit";
import Rightview from "../Rightview";
const Leftview = () => {
  const [show, setShow] = useState(false);
  const [values, setvalues] = useState("");
  const [arr, setArr] = useState([]);
  const [views, setviews] = useState([]);
  const [editState, setEditState] = useState(false);
  const [selectedId, setSelectedId] = useState(0);
  const [duplicate, setDuplicate] = useState(false);

  const handleClose = () => setShow(false);
  const handleclick = () => {
    setShow(!show);
  };
  const handlechange = (event) => {
    setvalues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };
  const submitform = () => {
    if (editState) {
      setArr(
        arr.map((item, index) => {
          return index === selectedId ? values : item;
        })
      );
    } else {
      const newobjedata = values;
      let arD = arr.filter((item) => {
        return item.phonenumber === newobjedata.phonenumber;
      });
      const newarry = [...arr, newobjedata];
      console.log(newarry);
      if (arD.length > 0) {
        alert("Duplicate Contact");
      } else {
        if (newobjedata !== "") {
          setArr(newarry);
          setvalues("");
          setShow(!show);
        } else {
          alert("fill the details");
        }
      }
    }
    setDuplicate(false);
    setEditState(false);
  };
  const handleEdit = (e) => {
    let id = e.target.id;
    let contact = arr[id];
    setSelectedId(Number(id));
    setEditState(true);
    setShow(!show);
    setvalues(contact);
  };
  const handleview = (e) => {
    if (e.target.checked) {
      let idd = e.target.id;
      let view = arr[idd];
      setviews([view]);
    }
  };
  const getRandomColor = () => {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };
  return (
    <Container fluid className="mt-5 pl-5">
      <Row>
        <Col xs="12" md="6">
          <div className="d-flex">
            <div className="d-flex pr-4">
              {" "}
              <Notebook width="45" height="45" className=" mr-3" />
              <div>
                <Text as="h4">Contacts</Text>
                <Text as="p">Welcome to Flat CRM Contact page</Text>
              </div>
            </div>

            <Text as="h5" className=" pl-4">
              sortby:Date created
            </Text>
          </div>
          <div fluid className="mt-4 mb-5 pl-5" style={{ display: "flex" }}>
            <Form style={{ width: "70%" }}>
              <Form.Control
                type="text"
                placeholder="Searchcontact"
                style={{ width: "354px", borderRadius: "20px" }}
              ></Form.Control>
            </Form>
            <Button className="ml-3 backgroundV" onClick={handleclick}>
              + Add Contact
            </Button>
          </div>
          <div>
            <table className="table table-borderless">
              <thead className="thead-light">
                <tr>
                  <th>+</th>
                  <th>BasicInfo</th>
                  <th>Company</th>
                  <th>Edit</th>
                </tr>
              </thead>
              <tbody>
                {arr.map((item, index) => {
                  return (
                    <tr key={index + 1}>
                      <td>
                        <Checkbox
                          id={index}
                          onClick={handleview}
                          checked={false}
                        />
                      </td>
                      <td>
                        <div className="flex_con">
                          <p
                            style={{
                              background: getRandomColor(),
                              borderRadius: "50%",
                              padding: "16px",
                              height: "55px",
                              width: "55px",
                              textAlign: "center",
                            }}
                          >
                            {item.firstname
                              .split(" ")
                              .map((name) => name.charAt(0))
                              .join("")}
                          </p>
                          <div className="flex_col">
                            <p>{item.firstname}</p>
                            <p>{item.email}</p>
                          </div>
                        </div>
                      </td>
                      <td>{item.companyname}</td>
                      <td>
                        <Edit id={index} onClick={handleEdit} />
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </Col>
        <Col xs="12" md="5">
          <Rightview views={views} />
        </Col>
      </Row>

      <Modal show={show} onHide={handleClose}>
        <Modal.Body>
          <Form>
            <Form.Control
              type="text"
              placeholder="Fullname"
              name="firstname"
              value={values.firstname}
              onChange={handlechange}
              className="mb-2"
            />
            <Form.Control
              type="email"
              placeholder="Email"
              name="email"
              value={values.email}
              onChange={handlechange}
              className="mb-2"
            />
            <Form.Control
              type="number"
              placeholder="Phone number"
              name="phonenumber"
              value={values.phonenumber}
              onChange={handlechange}
              className="mb-2"
            />
            <Form.Control
              type="text"
              placeholder="Designation"
              name="designation"
              value={values.designation}
              onChange={handlechange}
              className="mb-2"
            />
            <Form.Control
              type="text"
              placeholder="Companyname"
              name="companyname"
              value={values.companyname}
              onChange={handlechange}
              className="mb-2"
            />

            <Form.Control
              type="text"
              placeholder="Company Address"
              name="Companyaddress"
              value={values.Companyaddress}
              onChange={handlechange}
              className="mb-2"
            />

            <Button onClick={submitform}>submit</Button>
          </Form>
        </Modal.Body>
      </Modal>
    </Container>
  );
};

export default Leftview;
