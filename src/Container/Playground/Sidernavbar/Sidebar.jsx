import React from "react";
import Navbar from "../../../Component/Navbar";
import Nav from "../../../Component/Nav";
import styles from "./styles.module.css";
import classnames from "classnames";

import { ReactComponent as HomeIcon } from "../../../Component/Icons/home.svg";
import { ReactComponent as User } from "../../../Component/Icons/user.svg";
import { ReactComponent as PdfIcon } from "../../../Component/Icons/pdf.svg";
import { ReactComponent as Clock } from "../../../Component/Icons/clock.svg";
import { ReactComponent as Calendar } from "../../../Component/Icons/calendar.svg";
import { ReactComponent as Setting } from "../../../Component/Icons/setting.svg";
// import { ReactComponent as Menu } from "../../../Component/Icons/menu.svg";
function Sidebar() {
  const sidebarstyle = classnames(styles["sidebar"]);
  const navstyle = classnames(styles["navlink"]);
  return (
    <div className={sidebarstyle}>
      <Navbar>
        {/* <Navbar.Brand>
          <Menu width="25" height="25" />
        </Navbar.Brand> */}
        <Nav
          className={navstyle}
          style={{ display: "flex", flexDirection: "column" }}
        >
          <Nav.Link href="/" className="pb-5 d-flex">
            <HomeIcon width="25" height="25" />
          </Nav.Link>
          <Nav.Link className="pb-5 d-flex">
            <User width="25" height="25" />
          </Nav.Link>
          <Nav.Link className="pb-5 d-flex">
            <PdfIcon width="25" height="25" />
          </Nav.Link>

          <Nav.Link className="pb-5 d-flex">
            <Clock width="25" height="25" />
          </Nav.Link>
          <Nav.Link className="pb-5 d-flex">
            <Calendar width="25" height="25" />
          </Nav.Link>
          <Nav.Link className="pb-5 d-flex">
            <Calendar width="25" height="25" width="25" height="25" />
          </Nav.Link>
          <Nav.Link className="pb-5 d-flex">
            <Setting width="25" height="25" />
          </Nav.Link>
        </Nav>
      </Navbar>
    </div>
  );
}

export default Sidebar;
