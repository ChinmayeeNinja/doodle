import React from "react";
import styles from "./styles.module.css";

const Header = ({ className, children }) => {
  return (
    <div className={`${styles["header--container"]} ${className}`}>
      {children}
    </div>
  );
};

export default Header;
