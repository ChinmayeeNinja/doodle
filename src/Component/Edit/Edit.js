import React from "react";
function Edit(props) {
  return (
    <div>
      <span onClick={props.onClick} id={props.id}>
        Edit
      </span>
    </div>
  );
}

export default Edit;
