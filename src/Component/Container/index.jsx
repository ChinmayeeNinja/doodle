import React from "react";
import { Container as BootstrapContainer } from "react-bootstrap";

const Container = ({ children, fluid, className }) => {
  return (
    <BootstrapContainer fluid={fluid} className={className}>
      {children}
    </BootstrapContainer>
  );
};

export default Container;
