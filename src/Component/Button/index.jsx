import React from "react";
import classnames from "classnames";
import BootstrapButton from "react-bootstrap/Button";

const Button = ({
  className,

  children,

  size = "sm",

  onClick,
  ...props
}) => {
  const classes = classnames(className);
  return (
    <BootstrapButton className={classes} onClick={onClick} {...props}>
      {children}
    </BootstrapButton>
  );
};

export default Button;
