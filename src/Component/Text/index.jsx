import React from "react";
import classnames from "classnames";

const Text = ({ children, as }) => {
  const CustomTag = as || "span";
  return <CustomTag>{children}</CustomTag>;
};

export default Text;
