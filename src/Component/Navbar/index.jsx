import React from "react";
import BootstrapNavbar from "react-bootstrap/Navbar";

const Navbar = ({ bg, expand, children, ...props }) => {
  return (
    <BootstrapNavbar
      bg={bg}
      expand={expand}
      {...props}
      style={{ justifyContent: "space-between" }}
    >
      {children}
    </BootstrapNavbar>
  );
};

const Brand = ({ children, ...props }) => {
  return <BootstrapNavbar.Brand {...props}>{children}</BootstrapNavbar.Brand>;
};

const Toggle = ({ ...props }) => {
  return <BootstrapNavbar.Toggle {...props}></BootstrapNavbar.Toggle>;
};

const Collapse = ({ children, ...props }) => {
  return (
    <BootstrapNavbar.Collapse {...props}>{children}</BootstrapNavbar.Collapse>
  );
};

Navbar.Brand = Brand;
Navbar.Toggle = Toggle;
Navbar.Collapse = Collapse;

export default Navbar;
