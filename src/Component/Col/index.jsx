import React from "react";
import BootstrapCol from "react-bootstrap/Col";

const Col = ({ children, ...props }) => {
  return <BootstrapCol {...props}>{children}</BootstrapCol>;
};

export default Col;
