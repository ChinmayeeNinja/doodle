import React, { forwardRef } from "react";
//import classnames from "classnames";
import BootstrapForm from "react-bootstrap/Form";

const Form = forwardRef(({ children, as, inline, validated }, ref) => {
  // const formStyle = classnames(className, styles["form"]);
  return (
    <BootstrapForm ref={ref} as={as} inline={inline} validated={validated}>
      {children}
    </BootstrapForm>
  );
});

const Control = forwardRef(
  ({ type, placeholder, name, value, ...props }, ref) => {
    // const controlStyles = classnames(styles["text-input"]);
    return (
      <BootstrapForm.Control
        type={type}
        placeholder={placeholder}
        name={name}
        value={value}
        {...props}
      />
    );
  }
);

Form.Control = Control;

export default Form;
