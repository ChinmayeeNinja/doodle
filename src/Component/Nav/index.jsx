import React from "react";
import BootstrapNav from "react-bootstrap/Nav";

const Nav = ({ children, className, ...props }) => {
  return (
    <BootstrapNav className={className} {...props}>
      {children}
    </BootstrapNav>
  );
};

const Link = ({ href, className, children, ...props }) => {
  return (
    <BootstrapNav.Link href={href} className={className} {...props}>
      {children}
    </BootstrapNav.Link>
  );
};

Nav.Link = Link;

export default Nav;
