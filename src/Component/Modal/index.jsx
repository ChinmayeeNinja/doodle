import React from "react";
import BootstrapModal from "react-bootstrap/Modal";
const Modal = ({ children, show, onHide }) => {
  return (
    <BootstrapModal show={show} onHide={onHide}>
      {children}
    </BootstrapModal>
  );
};
const Header = ({ children }) => {
  return <BootstrapModal.Header closeButton>{children}</BootstrapModal.Header>;
};
const Title = ({ children }) => {
  return <BootstrapModal.Title>{children}</BootstrapModal.Title>;
};
const Body = ({ children }) => {
  return <BootstrapModal.Body>{children}</BootstrapModal.Body>;
};
const Footer = ({ children }) => {
  return <BootstrapModal.Footer>{children}</BootstrapModal.Footer>;
};

Modal.Header = Header;
Modal.Title = Title;
Modal.Body = Body;
Modal.Footer = Footer;
export default Modal;
